// // /*  Инициализация модуля связи кластера */ // //

/* Подключение и определение необходимого для самого ESP32 */
// Подключение необходимых библиотек для ESP32
#include "esp_task_wdt.h"  // Библиотека для работы с сторожевым таймером

#include <WiFi.h>        // Библиотека для работы с WiFi
#include <HTTPClient.h>  // Библиотека для работы в формате клиент-сервер

// Константы для ESP32
#define WDT_TIMEOUT 120  // Таймаут сторожевого таймера в секундах

// Константы для периодической переинициализации
#define INIT_PERIOD 120000   // Период переинициализации считывателей (в миллисекундах)
uint64_t _initLastTime = 0;  // Время последней инициализации считывателей (в миллисекундах)

// Константы для определения задержек (прерываний) в работе системы
#define DELAY_MIN 100    // Время задержки в работе системы между разными внутренними событиями (маленькая - для внутренней работы)
#define DELAY_MID 2500   // Время задержки в работе системы между разными внутренними событиями (средняя - для вывода сообщений)
#define DELAY_BIG 15000  // Время задержки в работе системы между разными внутренними событиями (большая - для вывода важных сообщений)

// "Сигнализация" о событиях
#define BLINK_INIT 1    // Сколько раз "моргать" при инициализации той или иной функции
#define BLINK_SETUP 5   // Сколько раз "моргать" при завершении инициализации устройства
#define BLINK_READ 2    // Сколько раз "моргать" при чтении метки
#define BLINK_REMOVE 3  // Сколько раз "моргать" при убирании метки

// Константы для Wi-Fi
#define WIFI_SSID "AKMG"        // SSID (имя) WiFi сети //(старое) MM
#define WIFI_PASS "1234567890"  // Пароль WiFi сети     //(старое) Qwertyui

// Константы для настройки сети
#define LOCAL_IP "192.168.1.11"      // Статический IP-адрес ESP32
#define GATEWAY_IP "192.168.1.1"     // IP-адрес шлюза (Gateway). Обычно это IP-адрес роутера. Шлюз используется для отправки трафика из вашей локальной сети в интернет.
#define DNS_IP "192.168.1.1"         // IP-адрес сервера доменных имен (DNS). Обычно это также IP-адрес роутера, если вы не используете отдельный DNS-сервер. \
                                     // DNS используется для перевода доменных имен (например, www.google.com) в IP-адреса.
#define SUBNET_MASK "255.255.255.0"  // Маска подсети (Subnet mask). Она определяет, какие IP-адреса в вашей сети считаются локальными. \
                                     // В обычных домашних сетях, где используются адреса 192.168.x.x, обычно используется маска подсети 255.255.255.0.

// Константы для сервера
#define SERVER_IP "192.168.1.10"     // IP-адрес сервера, на который будут отправляться данные
#define SERVER_PORT "5000"           // Порт сервера
#define SERVER_PATH "/api/endpoint"  // Путь на сервере, куда будут отправляться данные (так называемый "endpoint")
#define SERVER_apiKEY "ваш_api_key"  // Ключ API для сервера (если нужен)


/* Подключение и определение необходимого для считывателей */
// Подключение необходимых библиотек для считывателей
#include <SPI.h>      // Библиотека для работы с SPI
#include <MFRC522.h>  // Библиотека для работы с RC522

// Константы для считывателей
// МАСШТАБИРУЕМОСТЬ: Указываем нужное количество считывателей
#define NFC_numREADERS 2  // Количество считывателей

#define NFC_timeCHECK 50  // Количество проверок на наличие метки (во избежание ложных срабатываний)


/* Подключение и определение необходимого для экрана */
// Подключение необходимых библиотек для экрана
#include <Wire.h>              // Библиотека для работы с I2C
#include <Adafruit_GFX.h>      // Библиотека Adafruit GFX
#include <Adafruit_SSD1306.h>  // Библиотека Adafruit SSD1306

// Константы для экрана
#define SCREEN_WIDTH 128  // Ширина OLED-дисплея
#define SCREEN_HEIGHT 64  // Высота OLED-дисплея
#define OLED_RESET -1     // На большинстве модулей OLED нет сигнала RESET

#define TEXT_SIZE 1    // Задаём размер шрифта
#define TEXT_WIDTH 21  // Количество символов, помещающихся на одной строке при заданном шрифте
#define TEXT_HEIGHT 8  // Количество строк, помещающихся на экране

const uint8_t const_fontHeight = SCREEN_HEIGHT / TEXT_HEIGHT;  // Высота одной строки (высота шрифта) - считая, что 1 строка занимает 1/TEXT_HEIGHT высоты экрана
const uint8_t const_fontWidth = SCREEN_WIDTH / TEXT_WIDTH;     // Ширина одного символа (ширина шрифта) - считая, что 1 символ занимает 1/TEXT_WIDTH часть ширины экрана

uint64_t _messageDisplayTime;  // Время отображения сообщения (в миллисекундах)


/* Основные выводимые сообщения */
const char* MainText = "\n...Модуль работает...\n\n*** *** ** ** *** ***\n\n..Приложите вашу NFC-метку к считывателю..";  // Текст на главном экране


/* Определение структур и создание объектов */
// Структура для удобного хранения данных о каждом считывателе
struct Reader {
  MFRC522 mfrc522;   // Объект считывателя
  uint8_t rstPin;    // Pin rst
  uint8_t ssPin;     // Pin ss
  String lastUid;    // Строка для хранения последнего считанного UID в HEX
  String lastUid10;  // Строка для хранения последнего считанного UID в DEC
  bool isPresent;    // Флаг наличия метки

  // Конструктор структуры, принимает пины SS (это SDA) и RST
  Reader(uint8_t ssPin, uint8_t rstPin)
    : mfrc522(ssPin, rstPin), rstPin(rstPin), ssPin(ssPin), lastUid(""), lastUid10(""), isPresent(false) {}
};

// Создание массива считывателей
// МАСШТАБИРУЕМОСТЬ: Задаём здесь подключения считывателей - (SDA, RST)
Reader _readers[NFC_numREADERS] = {
  Reader(16, 5),
  Reader(17, 5),
  //Reader(17, 22)
};

// Создание объекта экрана
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

// // /*  Инициализация модуля связи кластера (конец) */ // //


// // /*  Функции для работы модуля связи кластера */ // //

/* Основные функции */
/* void setup - это самая первая функция
// Она вызывается (исполняется) когда на плату Arduino подается питание */
void setup() {

  pinMode(LED_BUILTIN, OUTPUT);  // Инициализация встроенного светодиода
  blink(BLINK_INIT);             // Сигнализация об инициализации светодиода

  initSerial();  // Инициализация serial port
  initOled();    // Инициализация OLED дисплея (по интерфейсу I2C)

  initWDT();           // Проводим инициализацию сторожевого таймера
  printResetReason();  // Обработка (пере)загрузки

  initWiFi();     // Инициализация WiFi
  initReaders();  // Инициализация считывателей

  dynamicMessage(F("Модуль связи кластера"), F("Полностью загружен"), '*', DELAY_MID);  // Вывод приветственного сообщения
  blink(BLINK_SETUP);                                                                   // Сигнализация о загрузке модуля
}

// void loop - это основная функция
// Она будет выполняться бесконечно, после функции setup, пока не сядет батарейка, или плата не будет перезагружена.
void loop() {
  // Контролируем состояние ESP32 и компонентов
  esp_task_wdt_reset();  // Обновляем сторожевой таймер

  // Проверяем, не пора ли переинициализировать систему (считыватели + WiFi)
  initResetup();

  // Обновляем графическое отображение
  printClear();  // Очищаем экран и выводим главное сообщение

  // Осуществляем проверку считывателей на наличие метки и считываем её (если есть)
  for (int i = 0; i < NFC_numREADERS; i++) {  // Для каждого считывателя
    checkReader(_readers[i], i);              // Проверяем, прочитал ли он новую метку
  }
}


/* Функции инициализации */
// Функция инициализации Serial port
void initSerial() {
  Serial.begin(9600);  // Инициализация серийного порта с скоростью 9600 бод
  while (!Serial)
    ;                 // Для ESP32, чтобы дождаться инициализации
  blink(BLINK_INIT);  // Сигнализируем об успешной инициализации Serial port
}

// Функция инициализации OLED экрана (по интерфейсу I2C)
void initOled() {
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {  // Инициализация с I2C адресом 0x3C
    Serial.println(F("SSD1306 (OLED): сбой инициализации"));
    for (;;)
      ;  // Если инициализация не удалась, зацикливаемся (не тестировалось)
  }
  display.cp437(true);     // Ставим русскую кодировку
  display.clearDisplay();  // Очищаем экран
  display.display();       // Обновляем экран

  display.setTextSize(TEXT_SIZE);  // Задаем размер текста
  display.setTextColor(WHITE);     // Задаем цвет текста

  dynamicMessage(F("ЗАГРУЗКА МОДУЛЯ"), F("Инициализация  OLED"), F("УСПЕШНО"), DELAY_MIN);
  blink(BLINK_INIT);  // Сигнализируем об успешной инициализации Wi-Fi
}

// Функция инициализации сторожевого таймера
void initWDT() {
  esp_task_wdt_init(WDT_TIMEOUT, true);  // Инициализируем сторожевой таймер
  esp_task_wdt_add(NULL);                // Добавляем в сторожевой таймер основной цикл loop()

  dynamicMessage(F("ЗАГРУЗКА МОДУЛЯ"), F("Инициализация WDT"), F("УСПЕШНО"), DELAY_MIN);
  blink(BLINK_INIT);  // Сигнализируем об инициализации сторожевого таймера
}

// Функция вывода причины перезагрузки
void printResetReason() {
  // Узнаваем причину перезагрузки
  esp_reset_reason_t reason = esp_reset_reason();

  // Выводим причину
  switch (reason) {
    case ESP_RST_WDT:  // Сброс по сторожевому таймеру
      dynamicMessage(F("ЗАГРУЗКА МОДУЛЯ"), F("Причина ресета"), F("Сторожевой таймер"), DELAY_MID);
      break;
    case ESP_RST_DEEPSLEEP:  // Сброс после выхода из режима глубокого сна
      dynamicMessage(F("ЗАГРУЗКА МОДУЛЯ"), F("Причина ресета"), F("Режим глубокого сна"), DELAY_MID);
      break;
    case ESP_RST_BROWNOUT:  // Сброс из-за просадки напряжения
      dynamicMessage(F("ЗАГРУЗКА МОДУЛЯ"), F("Причина ресета"), F("Просадка напряжения"), DELAY_MID);
      break;
    case ESP_RST_SW:  // Программный сброс (вызван esp_restart() или подобной функцией)
      dynamicMessage(F("ЗАГРУЗКА МОДУЛЯ"), F("Причина ресета"), F("Программный сброс"), DELAY_MID);
      break;
    case ESP_RST_PANIC:  // Сброс из-за ошибки в коде (panic)
      dynamicMessage(F("ЗАГРУЗКА МОДУЛЯ"), F("Причина ресета"), F("Ошибка в коде"), DELAY_MID);
      break;
    case ESP_RST_INT_WDT:  // Сброс из-за аппаратного сторожевого таймера
      dynamicMessage(F("ЗАГРУЗКА МОДУЛЯ"), F("Причина ресета"), F("Аппаратный таймер"), DELAY_MID);
      break;
    case ESP_RST_TASK_WDT:  // Сброс из-за сторожевого таймера задач
      dynamicMessage(F("ЗАГРУЗКА МОДУЛЯ"), F("Причина ресета"), F("Таймер  задач"), DELAY_MID);
      break;
    case ESP_RST_EXT:  // Сброс по внешнему сигналу (not applicable for ESP32)
      dynamicMessage(F("ЗАГРУЗКА МОДУЛЯ"), F("Причина ресета"), F("Внешний  сигнал"), DELAY_MID);
      break;
    case ESP_RST_SDIO:  // Сброс по SDIO
      dynamicMessage(F("ЗАГРУЗКА МОДУЛЯ"), F("Причина ресета"), F("Сброс по SDIO"), DELAY_MID);
      break;
    case ESP_RST_POWERON:  // Включение по подаче питания
      dynamicMessage(F("ЗАГРУЗКА МОДУЛЯ"), F("Причина ресета"), F("Подача питания/кнопка"), DELAY_MID);
      break;
    case ESP_RST_UNKNOWN:  // Сброс по неизвестной причине
      dynamicMessage(F("ЗАГРУЗКА МОДУЛЯ"), F("Причина ресета"), F("Неизвестно!"), DELAY_MID);
      break;
    default:  // Сброс по СОВСЕМ неизвестной причине
      dynamicMessage(F("ЗАГРУЗКА МОДУЛЯ"), F("Причина ресета"), F("Неизвестно!"), DELAY_MID);
      break;
  }

  blink(BLINK_INIT);  // Сигнализируем о выводе сообщения о причинах (пере)загрузки
}

// Функция для инициализации WiFi
void initWiFi() {

  // Инициализация параметров подключения
  IPAddress local_IP;  // Инициализация статического IP адреса ESP32
  local_IP.fromString(LOCAL_IP);

  IPAddress gateway;  // Инициализация Gateway
  gateway.fromString(GATEWAY_IP);

  IPAddress subnet;  // Инициализация Subnet mask
  subnet.fromString(SUBNET_MASK);

  IPAddress dns;  // Инициализация DNS
  dns.fromString(DNS_IP);

  // Установка статического IP-адреса до подключения к WiFi
  if (!WiFi.config(local_IP, gateway, subnet, dns)) {
    dynamicMessage(F("ОШИБКА"), F("Подключение к Wi-Fi"), F("Не удалось настроить STA"), DELAY_MID);
  }

  // Подключение к WiFi сети
  while (WiFi.status() != WL_CONNECTED) {
    WiFi.begin(WIFI_SSID, WIFI_PASS);  // Инициализация WiFi
    dynamicMessage(F("Подключение к Wi-Fi"), "SSID: " + String(WIFI_SSID), '*', DELAY_MID);
  }

  dynamicMessage(F("УСПЕШНО"), F("Подключение к Wi-Fi"), "IP: " + WiFi.localIP().toString(), DELAY_MID);
  blink(BLINK_INIT);  // Сигнализация об успешном подключении к Wi-Fi
}

// Функция для запуска и перезапуска считывателей
/*  Примечание: Данная функция была выведена эмперическим путём.
    При другом способе инициализации один из считывателей периодически
    (при одном из запусков) отказывался считывать метки */
void initReaders() {
  SPI.begin();  // Инициализация SPI-интерфейса

  for (int i = 0; i < NFC_numREADERS; i++) {  // Для каждого считывателя
    _readers[i].lastUid = "";                 // "Забываем" старые UID
    _readers[i].lastUid10 = "";

    pinMode(_readers[i].rstPin, OUTPUT);                                                               // Задаем pin rstPin как OUTPUT
    digitalWrite(_readers[i].rstPin, LOW);                                                             // Проводим перезагрузку (reset) считывателя
    dynamicMessage(F("ИНИЦИАЛИЗАЦИЯ NFC"), "Сброс ридера " + String(i + 1), F("УСПЕШНО"), DELAY_MIN);  // Задержка с выводом сообщения
    digitalWrite(_readers[i].rstPin, HIGH);                                                            // Возвращаем pin rstPin в HIGH
    _readers[i].mfrc522.PCD_Init();                                                                    // Инициализируем считыватель
  }
  for (int i = 0; i < NFC_numREADERS; i++) {                                                                 // Для каждого считывателя
    _readers[i].mfrc522.PCD_PerformSelfTest();                                                               // Проводим самотестирование (я не уверен, что эта функция работает...)
    _readers[i].mfrc522.PCD_Init();                                                                          // Инициализируем считыватель
    dynamicMessage(F("ИНИЦИАЛИЗАЦИЯ NFC"), "Подключение ридера " + String(i + 1), F("УСПЕШНО"), DELAY_MIN);  // Задержка с выводом сообщения
  }

  dynamicMessage(F("ЗАГРУЗКА МОДУЛЯ"), F("Инициализация NFC"), F("УСПЕШНО"), DELAY_MIN);
  blink(BLINK_INIT);  // Сигнализируем о (пере)инициализации считывателей
}

// Функция переинициализации
void initResetup() {
  // Проверяем, не пришло ли время переинициализации
  if (millis() - _initLastTime >= INIT_PERIOD) {
    dynamicMessage(F("ПЕРЕЗАГРУЗКА МОДУЛЯ"), F("Обновление ридеров"), '*', DELAY_MID);

    initWiFi();     // Инициализируем WiFi
    initReaders();  // Инициализируем считыватели

    dynamicMessage(F("ПЕРЕЗАГРУЗКА МОДУЛЯ"), F("Выполнена успешно"), '*', DELAY_MID);
    _initLastTime = millis();  // Обновляем время последней инициализации
  }
}


/* Декоративные функции */
// Функция для мигания светодиодом на ESP32
void blink(uint8_t numBlinks) {
  for (int i = 0; i < numBlinks; i++) {  // Для каждого считывателя
    digitalWrite(LED_BUILTIN, HIGH);     // turn the LED on (HIGH is the voltage level)
    delay(DELAY_MIN);                    // Продолжительность одного "мигания"
    digitalWrite(LED_BUILTIN, LOW);      // turn the LED off by making the voltage LOW
    delay(DELAY_MIN);                    // Пауза перед предполагаемым следующим "миганием"
  }
}

// Функция вывода на OLED экран и на serial port
void printMessage(const String& message) {
  display.clearDisplay();             // Очищаем экран
  display.setCursor(0, 0);            // Устанавливаем курсор в начало
  display.println(utf8rus(message));  // Выводим сообщение, прредварительно преобразовав кирилицу в читаемый вид
  display.display();                  // Обновляем экран

  Serial.println(message);  // Выводим сообщение в serial port

  _messageDisplayTime = millis();  // Запоминаем время вывода сообщения
}

// Функция очистки OLED экрана
void printClear() {

  // Проверяем, прошло ли достаточно времени с момента вывода сообщения
  if (_messageDisplayTime != 0 && millis() - _messageDisplayTime >= DELAY_BIG) {
    printMessage(MainText);  // Сообщение по умолчанию - главный экран
    _messageDisplayTime = 0;
  }
}

// Функция вывода на экран динамического сообщения (см. ниже перегрузки) (до 21 символа на строку)
void dynamicMessage(const char* text0, const char* text1, const char* text2, int displayTime, char fillChar = '\0') {

  uint64_t startTime = millis();  // Запоминаем время начала

  uint8_t verticalOffset = 4;                   // Определяем отступы верхней и нижней строк от центральной (в строках)
  uint8_t borderOffset = const_fontHeight / 2;  // Определяет размер рамки
  uint8_t borderMove = 1 * const_fontHeight;    // Определяет смещение (в строках)

  for (int i = 0; i <= TEXT_WIDTH; i++) {                            // Проходим по всем символам
    while (millis() - startTime < i * (displayTime / TEXT_WIDTH)) {  // Ждем, пока не придет время для следующего символа
      // Ничего не делаем
    }
    display.clearDisplay();  // Очищаем экран

    // Печать верхнего текста
    if (strlen(text1) <= TEXT_WIDTH) {
      display.setCursor(((TEXT_WIDTH - strlen(text1)) / 2) * const_fontWidth, (TEXT_HEIGHT / 2 - verticalOffset) * const_fontHeight);
    } else {
      display.setCursor(0, (TEXT_HEIGHT / 2 - verticalOffset) * const_fontHeight);
    }
    display.println(text1);


    if (fillChar == '\0') {  // Если символ для заполнения не предоставлен, используем метод рисования прямоугольника

      // Рисуем прямоугольник, который заполняется по мере прохождения времени
      display.drawRoundRect(0, (TEXT_HEIGHT / 2) * const_fontHeight - borderOffset - borderMove, const_fontWidth * i, const_fontHeight + borderOffset * 2, 0, WHITE);
      if (i == TEXT_WIDTH) {  // Если рамка полностью заполнилась
        // Закрашиваем прямоугольник
        display.fillRoundRect(0, (TEXT_HEIGHT / 2) * const_fontHeight + (borderOffset / 2) - borderMove, const_fontWidth * i, const_fontHeight - (borderOffset / 2) * 2, 0, WHITE);
        // Изменяем цвет текста на черный
        display.setTextColor(BLACK, WHITE);
      }

      // Выводим текст посередине
      display.setCursor((SCREEN_WIDTH - strlen(text0) * const_fontWidth) / 2, (TEXT_HEIGHT / 2) * const_fontHeight - borderMove);
      display.println(text0);

      display.setTextColor(WHITE, BLACK);  // Возвращаем обратно цвет текста на белый

    } else {  // Иначе выводим строку с символами

      char fillString[i + 1];           // Массив символов для динамического вывода
      memset(fillString, fillChar, i);  // Заполняем массив символом fillChar
      fillString[i] = '\0';             // Добавляем в конец символ конца строки

      // Устанавливаем курсор с учетом отступов для динамической строки и печатаем её
      display.setCursor((SCREEN_WIDTH - strlen(fillString) * const_fontWidth) / 2, (TEXT_HEIGHT / 2) * const_fontHeight - borderMove);
      display.println(fillString);
    }

    // Устанавливаем курсор с учетом отступов для нижнего текста и печатаем его
    if (strlen(text2) <= TEXT_WIDTH) {
      display.setCursor(((TEXT_WIDTH - strlen(text2)) / 2) * const_fontWidth, (TEXT_HEIGHT / 2 + (verticalOffset / 2)) * const_fontHeight);
    } else {
      display.setCursor(0, (TEXT_HEIGHT / 2 + (verticalOffset / 2)) * const_fontHeight);
    }
    display.println(text2);

    display.display();  // Обновляем экран
  }
  _messageDisplayTime = 1;  // Флаг для последующего мгновенного перехода на главный экран
}
// Перегрузка функции динамического вывода для случая, если символ заполнения предоставлен
void dynamicMessage(const String& text1, const String& text2, char fillChar, int displayTime) {  // верх, низ, заполнитель строки, время вывода сообщения
  // Дублируем вывод в Serial port
  Serial.println("\n" + text1 + " : " + text2);

  // Выводим на OLED-экран (в динамическом формате)
  dynamicMessage("", utf8rus(text1).c_str(), utf8rus(text2).c_str(), displayTime, fillChar);
}
// Перегрузка функции динамического вывода для случая, если символ заполнения не предоставлен
void dynamicMessage(const String& text0, const String& text1, const String& text2, int displayTime) {  // центр, верх, низ, время вывода сообщения
                                                                                                       // Дублируем вывод в Serial port
  Serial.println("\n" + text0 + " : " + text1 + " : " + text2);

  // Выводим на OLED-экран (в динамическом формате
  dynamicMessage(utf8rus(text0).c_str(), utf8rus(text1).c_str(), utf8rus(text2).c_str(), displayTime, '\0');
}

// Функция переводит русский шрифт из UTF-8 в Windows-1251
String utf8rus(const String& source) {
  int i, k;
  String target = "";
  unsigned char n;
  char m[2] = { '0', '\0' };

  k = source.length();
  i = 0;

  while (i < k) {
    n = source[i];
    i++;

    if (n >= 0xC0) {
      switch (n) {
        case 0xD0:
          {
            n = source[i];
            i++;
            if (n == 0x81) {
              n = 0xA8;
              break;
            }
            if (n >= 0x90 && n <= 0xBF) n = n + 0x30;
            break;
          }
        case 0xD1:
          {
            n = source[i];
            i++;
            if (n == 0x91) {
              n = 0xB8;
              break;
            }
            if (n >= 0x80 && n <= 0x8F) n = n + 0x70;
            break;
          }
      }
    }
    m[0] = n;
    target = target + String(m);
  }
  return target;
}


/* Функции для NFC */
// Функция проверяет наличие метки у считывателя
bool isCardPresent(Reader& reader) {

  for (int i = 0; i < NFC_timeCHECK; i++) {  // Проверяем наличие метки NFC_timeCHECK раз
    // Если метка есть, возвращаем true
    if (reader.mfrc522.PICC_IsNewCardPresent() && reader.mfrc522.PICC_ReadCardSerial()) {
      return true;
    }

    delay(DELAY_MIN);  // Задержка между проверками в DELAY_MIN миллисекунд
  }
  return false;  // Если метка не найдена, возвращаем false
}

// Функция проверяет, была ли прочитана новая метка
void checkReader(Reader& reader, int readerIndex) {

  if (isCardPresent(reader)) {  // Если метка присутствует

    // Формирование строки с UID метки
    String content = "";                                                          // Строка для хранения UID метки в HEX
    String content10 = "";                                                        // Строка для хранения UID метки в DEC
    for (byte i = 0; i < reader.mfrc522.uid.size; i++) {                          // Для каждого байта UID
      content.concat(String(reader.mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));  // Добавляем его к строке, предварительно преобразовав в HEX
      content.concat(String(reader.mfrc522.uid.uidByte[i], HEX));
      content10.concat(String(reader.mfrc522.uid.uidByte[i]));
    }

    // Если это новая метка
    if (content != reader.lastUid) {

      if (reader.isPresent) {      // Если старая метка ещё числится за этим считывателем
        reader.isPresent = false;  // Обновляем флаг наличия метки
      }

      // Обеспечиваем однозначность закрепления метки за одним считывателем
      for (int i = 0; i < NFC_numREADERS; i++) {                   // Для каждого другого считывателя
        if (i != readerIndex && _readers[i].lastUid == content) {  // Если UID совпадает с UID последней прочитанной метки
          _readers[i].lastUid = "";                                // "Забываем" этот UID
          _readers[i].lastUid10 = "";
        }
      }

      // Закрепляем метку за данным считывателем
      reader.lastUid = content;  // Обновляем последний прочитанный UID
      reader.lastUid10 = content10;
    }

    // Если это новая метка или метка была ранее удалена
    if (!reader.isPresent) {
      reader.isPresent = true;  // Обновляем флаг наличия метки

      // Выводим сообщение с номером считывателя и UID метки
      printMessage("Считыватель #" + String(readerIndex + 1) + "\n -> Метка считана" + "\n\nUID (HEX) :\n" + content + "\nUID (DEC) :\n " + content10);
      //reader.mfrc522.PICC_DumpToSerial(&(reader.mfrc522.uid)); // Выводим дамп данных (считываем всё, что можно считать)

      // ТЕСТОВЫЙ УЧАСТОК: Отправка данных на сервер
      sendDataToServer(content);

      // Мигаем светодиодами
      blink(BLINK_READ);
    }
  } else {  // Если метка отсутствует

    if (reader.isPresent) {      // Если метка была на считывателе
      reader.isPresent = false;  // Обновляем флаг наличия метки

      if (reader.lastUid != "") {  // Если информация об этой метке ещё не была потёрта (т.е. актуальна)

        // Выводим сообщение о том, что метка была убрана
        printMessage("Считыватель #" + String(readerIndex + 1) + "\n -> Метка убрана" + "\n\nUID (HEX) :\n" + reader.lastUid + "\nUID (DEC) :\n " + reader.lastUid10);

        // Мигаем светодиодами
        blink(BLINK_REMOVE);
      }
    }
  }
}


/* Функции для работы с сетью */
// Функция для отправки данных на сервер
void sendDataToServer(const String& data) {
  // Проверяем статус подключения WiFi.
  if (WiFi.status() == WL_CONNECTED) {  // WL_CONNECTED означает, что устройство подключено к WiFi-сети

    // Создаем объект класса HTTPClient.
    HTTPClient http;  // Этот класс предоставляет функции для отправки HTTP-запросов

    // Определяем URL, куда отправлять HTTP-запрос.
    http.begin("http://" + String(SERVER_IP) + ":" + String(SERVER_PORT) + String(SERVER_PATH));  // URL формируется путем соединения IP-адреса и порта сервера, а также endpoint

    // Устанавливаем тип контента, который будет отправлен в HTTP-запросе.
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");  // "application/x-www-form-urlencoded" - стандартный тип контента для POST-запросов

    // Формируем данные, которые будут отправлены в POST-запросе.
    String httpRequestData = "api_key=" + String(SERVER_apiKEY) + "&data=" + data;  // Данные - строка вида "ключ=значение", где каждая пара разделена амперсандом (&)

    // Отправляем HTTP POST запрос на сервер и получаем HTTP код ответа.
    int httpResponseCode = http.POST(httpRequestData);  // HTTP коды ответа - это стандартные коды, которые серверы используют для передачи информации о статусе запроса

    if (httpResponseCode > 0) {  // Если код ответа больше 0, это означает, что сервер ответил на наш запрос. В этом случае мы читаем и выводим ответ сервера
      printMessage("Ответ сервера:\n" + http.getString());
    } else {                                                                   // Если код ответа не больше 0, это означает, что произошла ошибка при выполнении запроса.
      printMessage("Ошибка при отправке POST:\n" + String(httpResponseCode));  // Выводим сообщение об ошибке с кодом ответа
    }

    http.end();  // Завершаем соединение. Это освобождает ресурсы, которые использовались для установления соединения
  } else {
    // Если статус подключения WiFi не равен WL_CONNECTED, это означает, что устройство не подключено к WiFi-сети.
    printMessage(F("WiFi не подключен. Данные не отправлены."));  // Выводим сообщение об ошибке
  }
}

// // /*  Функции для работы модуля связи кластера (конец) */ // //
