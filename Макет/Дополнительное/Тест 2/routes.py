""" // // Файл для обработки маршрутов // // """

from flask import request
from server import app, db  # Импортируем объект Flask app и объект SQLAlchemy db из вашего основного файла

# Импортируем все классы для работы с таблицами (не можем использовать * во избежание конфликта с db)
from db.models import User, StorageZone, Cell, Container, Object, StoredContainer, StoredObject, EmptyContainer, WithdrawnObject, WrittenOffObject, Documentation, OperationHistory

""" Объявление констант """
# IP-адреса клиентов
# Словарь IP-адресов клиентов и соответствующих сообщений
CLIENT_IP_MESSAGES = {
    '192.168.1.11': 'Получены данные от модуля связи кластера ячеек №1: ',
    #'192.168.1.2': 'Получены данные от модуля связи кластера ячеек №2: '
}


""" Обработка запросов """
# Декоратор app.route создает маршрут URL для веб-приложения.
# В данном случае, когда наше веб-приложение получает POST-запрос по URL /api/endpoint, Flask вызывает функцию handle_post()
@app.route('/api/endpoint', methods=['POST'])
def handle_post():
    client_ip = request.remote_addr # Получаем IP-адрес клиента, который сделал этот запрос
    
    # Получаем сообщение для данного IP-адреса, или сообщение по умолчанию, если IP-адреса нет в словаре
    message = CLIENT_IP_MESSAGES.get(client_ip, f'Получены данные от неизвестного клиента ({client_ip}): ')
    print(message + str(request.form)) # Выводим сообщение с данными

    return 'OK', 200 # Отправляем ответ клиенту. В данном случае мы отправляем строку 'OK' и HTTP-код 200, который означает "успех".

    # data = request.form # Извлекаем данные из POST-запроса. В нашем случае они находятся в теле запроса
    # print(data)  # Выводим данные в консоль

    # Здесь мы добавим запись в базу данных
    # Но пока что этот код закомментирован
    # new_data = MyData(data=request.form['data'])
    # db.session.add(new_data)
    # db.session.commit()

""" // // Файл для обработки маршрутов (конец) // // """