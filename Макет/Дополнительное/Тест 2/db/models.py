""" // // Файл для создания моделей таблиц в БД // // """
# Примечание: модель БД создаётся именно здесь, но файл с БД создаётся в server.py
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy() # Создаём экземпляр SQLAlchemy для дальнейшей работы с БД

# Создание модели "Зона хранения"
class StorageZone(db.Model):
    __tablename__ = 'StorageZone'
    ID = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.Text, nullable=True)
    Purpose = db.Column(db.Text, nullable=True)
    Location = db.Column(db.Text, nullable=True)
    CellCount = db.Column(db.Integer, nullable=True)

# Создание модели "Ячейка"
class Cell(db.Model):
    __tablename__ = 'Cell'
    ID = db.Column(db.Integer, primary_key=True)
    NFC_Reader_Num = db.Column(db.Text, nullable=True)
    Max_Container_Weight = db.Column(db.Float, nullable=True)
    Max_Container_Dimensions = db.Column(db.Text, nullable=True)
    Location = db.Column(db.Text, nullable=True)
    Status = db.Column(db.Text, nullable=True)
    StorageZone_ID = db.Column(
        db.Integer, db.ForeignKey('StorageZone.ID'), nullable=True)

# Создание модели "Контейнер"
class Container(db.Model):
    __tablename__ = 'Container'
    ID = db.Column(db.Integer, primary_key=True)
    Type = db.Column(db.Text, nullable=True)
    Dimensions = db.Column(db.Text, nullable=True)
    Weight = db.Column(db.Float, nullable=True)
    Status = db.Column(db.Text, nullable=True)
    Registration_Date = db.Column(db.Text, nullable=True)
    Description = db.Column(db.Text, nullable=True)
    Max_Object_Weight = db.Column(db.Float, nullable=True)

# Создание модели "Объект"
class Object(db.Model):
    __tablename__ = 'Object'
    ID = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.Text, nullable=True)
    Category = db.Column(db.Text, nullable=True)
    Type = db.Column(db.Text, nullable=True)
    Dimensions = db.Column(db.Text, nullable=True)
    Weight = db.Column(db.Float, nullable=True)
    Status = db.Column(db.Text, nullable=True)
    Registration_Date = db.Column(db.Text, nullable=True)
    Description = db.Column(db.Text, nullable=True)

# Создание модели "Пользователь"
class User(db.Model):
    __tablename__ = 'User'
    ID = db.Column(db.Integer, primary_key=True)
    User_Type = db.Column(db.Text, nullable=True)
    Last_Name = db.Column(db.Text, nullable=True)
    First_Name = db.Column(db.Text, nullable=True)
    Middle_Name = db.Column(db.Text, nullable=True)
    Department = db.Column(db.Text, nullable=True)
    Login = db.Column(db.Text, nullable=True)
    Password = db.Column(db.Text, nullable=True)

# Создание модели "Хранящийся контейнер"
class StoredContainer(db.Model):
    __tablename__ = 'StoredContainer'
    StorageZone_ID = db.Column(db.Integer, db.ForeignKey(
        'StorageZone.ID'), primary_key=True)
    Cell_ID = db.Column(db.Integer, db.ForeignKey('Cell.ID'), primary_key=True)
    Container_ID = db.Column(db.Integer, db.ForeignKey(
        'Container.ID'), primary_key=True)
    Operation_ID = db.Column(db.Integer, primary_key=True)
    User_ID = db.Column(db.Integer, db.ForeignKey('User.ID'), primary_key=True)
    Storage_Start_Time = db.Column(db.Text, nullable=True)
    Object_Count = db.Column(db.Integer, nullable=True)

# Создание модели "Хранящийся объект"
class StoredObject(db.Model):
    __tablename__ = 'StoredObject'
    StorageZone_ID = db.Column(db.Integer, db.ForeignKey(
        'StorageZone.ID'), primary_key=True)
    Cell_ID = db.Column(db.Integer, db.ForeignKey('Cell.ID'), primary_key=True)
    Container_ID = db.Column(db.Integer, db.ForeignKey(
        'Container.ID'), primary_key=True)
    Object_ID = db.Column(db.Integer, db.ForeignKey(
        'Object.ID'), primary_key=True)
    Operation_ID = db.Column(db.Integer, primary_key=True)
    User_ID = db.Column(db.Integer, db.ForeignKey('User.ID'), primary_key=True)
    Storage_Start_Time = db.Column(db.Text, nullable=True)

# Создание модели "Пустой контейнер"
class EmptyContainer(db.Model):
    __tablename__ = 'EmptyContainer'
    Container_ID = db.Column(db.Integer, db.ForeignKey(
        'Container.ID'), primary_key=True)
    User_ID = db.Column(db.Integer, db.ForeignKey('User.ID'), primary_key=True)
    Operation_ID = db.Column(db.Integer, primary_key=True)
    Idle_Start_Time = db.Column(db.Text, nullable=True)

# Создание модели "Изъятый объект"
class WithdrawnObject(db.Model):
    __tablename__ = 'WithdrawnObject'
    Object_ID = db.Column(db.Integer, db.ForeignKey(
        'Object.ID'), primary_key=True)
    User_ID = db.Column(db.Integer, db.ForeignKey('User.ID'), primary_key=True)
    Operation_ID = db.Column(db.Integer, primary_key=True)
    Withdrawal_Time = db.Column(db.Text, nullable=True)
    Withdrawal_Reason = db.Column(db.Text, nullable=True)
    Withdrawal_Period = db.Column(db.Text, nullable=True)

# Создание модели "Списанный объект"
class WrittenOffObject(db.Model):
    __tablename__ = 'WrittenOffObject'
    Object_ID = db.Column(db.Integer, db.ForeignKey(
        'Object.ID'), primary_key=True)
    User_ID = db.Column(db.Integer, db.ForeignKey('User.ID'), primary_key=True)
    Operation_ID = db.Column(db.Integer, primary_key=True)
    WriteOff_Time = db.Column(db.Text, nullable=True)
    WriteOff_Reason = db.Column(db.Text, nullable=True)

# Создание модели "Документация"
class Documentation(db.Model):
    __tablename__ = 'Documentation'
    ID = db.Column(db.Integer, primary_key=True)
    Document_Name = db.Column(db.Text, nullable=True)
    Document_Type = db.Column(db.Text, nullable=True)
    Current_Location = db.Column(db.Text, nullable=True)
    Object_ID = db.Column(
        db.Integer, db.ForeignKey('Object.ID'), nullable=True)

# Создание модели "История операций"
class OperationHistory(db.Model):
    __tablename__ = 'OperationHistory'
    ID = db.Column(db.Integer, primary_key=True)
    Operation_Type = db.Column(db.Text, nullable=True)
    Time = db.Column(db.Text, nullable=True)
    Status = db.Column(db.Text, nullable=True)
    User_ID = db.Column(db.Integer, db.ForeignKey('User.ID'), nullable=True)
    StorageZone_ID = db.Column(
        db.Integer, db.ForeignKey('StorageZone.ID'), nullable=True)
    Cell_ID = db.Column(db.Integer, db.ForeignKey('Cell.ID'), nullable=True)
    Container_ID = db.Column(
        db.Integer, db.ForeignKey('Container.ID'), nullable=True)
    Object_ID = db.Column(
        db.Integer, db.ForeignKey('Object.ID'), nullable=True)


""" // // Файл для создания моделей таблиц в БД (конец) // // """
