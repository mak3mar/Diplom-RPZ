// Подключение необходимых библиотек
#include <Wire.h>     // Библиотека для работы с I2C
#include <SPI.h>      // Библиотека для работы с SPI
#include <MFRC522.h>  // Библиотека для работы с RC522

#define SS_PIN_1 16   // Назначение пина для SDA (сигнал Slave Select) первого считывателя
#define SS_PIN_2 17  // Назначение пина для SDA (сигнал Slave Select) второго считывателя
#define RST_PIN 5    // Назначение пина для RST (сброс)

MFRC522 mfrc522_1(SS_PIN_1, RST_PIN);  // Создание экземпляра объекта MFRC522 для первого считывателя
MFRC522 mfrc522_2(SS_PIN_2, RST_PIN);  // Создание экземпляра объекта MFRC522 для второго считывателя
String lastUid1, lastUid2;             // Строковые переменные для хранения последнего считанного UID каждым считывателем
String lastReader;                     // Строковая переменная для хранения информации о последнем считывателе

// void setup - это самая первая функция
// Она вызывается (исполняется) когда на плату Arduino подается питание
void setup() {
  Serial.begin(9600);  // Инициализация серийного порта с скоростью 9600 бод
  while (!Serial)
    ;  // Для ESP32, чтобы дождаться инициализации Serial

  SPI.begin();           // Инициализация SPI интерфейса
  mfrc522_1.PCD_Init();  // Инициализация первого считывателя RC522
  mfrc522_2.PCD_Init();  // Инициализация второго считывателя RC522

  Serial.println("Приложите вашу NFC-метку к RC522...");  // Вывод сообщения в сериальный порт
}

// void loop - это основная функция
// Она будет выполняться снова и снова, пока не сядет батарейка, или плата не будет перезагружена.
void loop() {
  checkReader(mfrc522_1, lastUid1, "Считыватель №1: ");  // Проверка первого считывателя
  checkReader(mfrc522_2, lastUid2, "Считыватель №2: ");  // Проверка второго считывателя
}

// Функция для считывания информации с метки
void checkReader(MFRC522& mfrc522, String& lastUid, String message) {
  if (!mfrc522.PICC_IsNewCardPresent()) {
    //Serial.println("Тест 1");
    return;  // Если новой карты нет, выходим из функции
  }

  if (!mfrc522.PICC_ReadCardSerial()) {
    //Serial.println("Тест 2");
    return;  // Если не удалось считать карту, выходим из функции
  }

  String content = "";
  for (byte i = 0; i < mfrc522.uid.size; i++) {
    // Добавляем в строку содержимое каждого байта UID
    content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
    content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }

  if (content == lastUid && message == lastReader) {
    return;  // Если UID и считыватель не изменились, выходим из функции
  }

  lastUid = content;     // Сохраняем новый считанный UID
  lastReader = message;  // Сохраняем информацию о последнем считывателе

  Serial.println(message + "UID тэга : " + content);  // Выводим информацию о считывателе и UID в сериальный порт
}
