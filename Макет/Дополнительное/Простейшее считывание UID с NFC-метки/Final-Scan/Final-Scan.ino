/* Подключение и определение необходимого для считывателей */
// Подключение необходимых библиотек для считывателей
#include <SPI.h>      // Библиотека для работы с SPI
#include <MFRC522.h>  // Библиотека для работы с RC522

// Объявляем константы для считывателей
// МАСШТАБИРУЕМОСТЬ: Указываем нужное количество считывателей
#define NUM_READERS 2   // Задаем количество считывателей
#define CHECK_TIMES 25  // Указываем количество проверок на наличие метки (во избежание ложных срабатываний)
#define CHECK_DELAY 100  // Указываем время задержки между проверками (для надёжности)
/* Подключение и определение необходимого для считывателей (конец)*/


/* Подключение и определение необходимого для экрана */
// Подключение необходимых библиотек для экрана
#include <Wire.h>             // Библиотека для работы с I2C
#include <Adafruit_GFX.h>     // Библиотека Adafruit GFX
#include <Adafruit_SSD1306.h> // Библиотека Adafruit SSD1306

// Объявляем константы для экрана
#define SCREEN_WIDTH 128            // Ширина OLED-дисплея
#define SCREEN_HEIGHT 64            // Высота OLED-дисплея
#define OLED_RESET    -1            // На большинстве модулей OLED нет сигнала RESET
#define TEXT_SIZE      1            // Задаём размер шрифта

const unsigned long DISPLAY_DURATION = 15000;  // Продолжительность отображения сообщения (в миллисекундах)
unsigned long messageDisplayTime;             // Время отображения сообщения
/* Подключение и определение необходимого для экрана (конец)*/


/* Основные выводимые сообщения */
const char *HelloText = "\n....Модуль загрузился и начал работать....\n\n*** *** ** ** *** ***\n\n..Приложите вашу NFC-метку к считывателю.."; // Текст приветствия
const char *MainText = "\n...Модуль работает...\n\n*** *** ** ** *** ***\n\n..Приложите вашу NFC-метку к считывателю.."; // Текст на главном экране
/* Основные выводимые сообщения (конец) */


/* Определение структур и создание объектов */
// Структура для удобного хранения данных о каждом считывателе
struct Reader {
  MFRC522 mfrc522;  // Объект считывателя
  String lastUid;   // Строка для хранения последнего считанного UID в HEX
  String lastUid10; // Строка для хранения последнего считанного UID в DEC
  bool isPresent;   // Флаг наличия метки

  // Конструктор структуры, принимает пины SS (это SDA) и RST
  Reader(uint8_t ssPin, uint8_t rstPin) : mfrc522(ssPin, rstPin), lastUid(""), lastUid10(""), isPresent(false) {}
};

// Создаем массив считывателей
// МАСШТАБИРУЕМОСТЬ: Задаём здесь подключения считывателей - (SDA, RST)
Reader readers[NUM_READERS] = {
  Reader(15, 5),
  Reader(16, 5),    // Заметка: На 17 пин лучше не выводить - периодически "отваливается..."
  //Reader(17, 22)
};

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET); // Создаем объект экрана
/* Определение структур и создание объектов (конец) */


/* Основные функции */
// void setup - это самая первая функция
// Она вызывается (исполняется) когда на плату Arduino подается питание
void setup() {
  // Инициализируем встроенный светодиод
  pinMode(LED_BUILTIN, OUTPUT);

  // Инициализация serial port
  Serial.begin(9600); // Инициализация серийного порта с скоростью 9600 бод
  while (!Serial);    // Для ESP32, чтобы дождаться инициализации 

  // Инициализация SPI интерфейса и NFC-считывателей
  SPI.begin(); 
  for (int i = 0; i < NUM_READERS; i++) { // Для каждого считывателя
    readers[i].mfrc522.PCD_Init(); // Инициализируем считыватель
  }

  // Инициализация OLED дисплея и интерфейса I2C
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {  // Инициализация с I2C адресом 0x3C
    Serial.println(F("SSD1306: сбой инициализации"));
    for(;;);                                        // Если инициализация не удалась, зацикливаемся (не тестировалось)
  }
  display.cp437(true);                              // Ставим русскую кодировку
  display.clearDisplay();                           // Очищаем экран
  display.display();                                // Обновляем экран

  display.setTextSize(TEXT_SIZE);                   // Задаем размер текста
  display.setTextColor(WHITE);                      // Задаем цвет текста

  // Выводим приветственное сообщение на экран
  printMessage(HelloText);
}

// void loop - это основная функция
// Она будет выполняться бесконечно, после функции setup, пока не сядет батарейка, или плата не будет перезагружена.
void loop() {
  printClear();
  
  for (int i = 0; i < NUM_READERS; i++) { // Для каждого считывателя
    checkReader(readers[i], i);           // Проверяем, прочитал ли он новую метку
  }
}
/* Основные функции (конец) */


/* Декоративные функции */
// Функция для мигания светодиодом на ESP32
void blink() {
      digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
      delay(CHECK_DELAY);                      // Ждём CHECK_DELAY миллисекунд
      digitalWrite(LED_BUILTIN, LOW);   // turn the LED off by making the voltage LOW
      delay(CHECK_DELAY);                      // Ждём CHECK_DELAY миллисекунд
      digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
      delay(CHECK_DELAY);                      // Ждём CHECK_DELAY миллисекунд
      digitalWrite(LED_BUILTIN, LOW);   // turn the LED off by making the voltage LOW
}

// Функция вывода на OLED экран и на serial port
void printMessage(String message) {
  display.clearDisplay();             // Очищаем экран
  display.setCursor(0,0);             // Устанавливаем курсор в начало
  display.println(utf8rus(message));  // Выводим сообщение, прредварительно преобразовав кирилицу в читаемый вид
  display.display();                  // Обновляем экран

  messageDisplayTime = millis();    // Запоминаем время вывода сообщения

  Serial.println(message);            // Выводим сообщение в serial port
}

// Функция очистки OLED экрана
void printClear() {

  // Проверяем, прошло ли достаточно времени с момента вывода сообщения
  if (messageDisplayTime != 0 && millis() - messageDisplayTime >= DISPLAY_DURATION) {
    printMessage(MainText); // Сообщение по умолчанию - главный экран
    messageDisplayTime =0;
  }

}

/* Recode russian fonts from UTF-8 to Windows-1251 */
String utf8rus(String source)
{
  int i,k;
  String target = "";
  unsigned char n;
  char m[2] = { '0', '\0' };

  k = source.length(); i = 0;

  while (i < k) {
    n = source[i]; i++;

    if (n >= 0xC0) {
      switch (n) {
        case 0xD0: {
          n = source[i]; i++;
          if (n == 0x81) { n = 0xA8; break; }
          if (n >= 0x90 && n <= 0xBF) n = n + 0x30;
          break;
        }
        case 0xD1: {
          n = source[i]; i++;
          if (n == 0x91) { n = 0xB8; break; }
          if (n >= 0x80 && n <= 0x8F) n = n + 0x70;
          break;
        }
      }
    }
    m[0] = n; target = target + String(m);
  }
return target;
}
/* Декоративные функции (конец) */


/* Функции для NFC */
// Функция проверяет наличие метки у считывателя
bool isCardPresent(Reader& reader) {
  for (int i = 0; i < CHECK_TIMES; i++) { // Проверяем наличие метки CHECK_TIMES раз

    // Если метка есть, возвращаем true
    if (reader.mfrc522.PICC_IsNewCardPresent() && reader.mfrc522.PICC_ReadCardSerial()) {
      return true;
    }

    delay(CHECK_DELAY); // Задержка между проверками в CHECK_DELAY миллисекунд
  }
  return false; // Если метка не найдена, возвращаем false
}

// Функция проверяет, была ли прочитана новая метка
void checkReader(Reader& reader, int readerIndex) {

  if (isCardPresent(reader)) {  // Если метка присутствует

    // Формирование строки с UID метки
    String content= "";         // Строка для хранения UID метки в HEX
    String content10= "";       // Строка для хранения UID метки в DEC
    for (byte i = 0; i < reader.mfrc522.uid.size; i++) {                         // Для каждого байта UID
      content.concat(String(reader.mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ")); // Добавляем его к строке, предварительно преобразовав в HEX
      content.concat(String(reader.mfrc522.uid.uidByte[i], HEX));
      content10.concat(String(reader.mfrc522.uid.uidByte[i])); 
    }

    // Если это новая метка
    if (content != reader.lastUid ) {
      
      if (reader.isPresent) {     // Если старая метка ещё числится за этим считывателем
        reader.isPresent = false; // Обновляем флаг наличия метки
      }

      // Обеспечиваем однозначность закрепления метки за одним считывателем
      for (int i = 0; i < NUM_READERS; i++) {                     // Для каждого другого считывателя
        if (i != readerIndex && readers[i].lastUid == content) {  // Если UID совпадает с UID последней прочитанной метки
          readers[i].lastUid = "";                                // "Забываем" этот UID
          readers[i].lastUid10 = "";
        }
      }

      // Закрепляем метку за данным считывателем
      reader.lastUid = content;   // Обновляем последний прочитанный UID
      reader.lastUid10 = content10;
    }

    // Если это новая метка или метка была ранее удалена
    if (!reader.isPresent ) {
      reader.isPresent = true; // Обновляем флаг наличия метки

      // Выводим сообщение с номером считывателя и UID метки
      printMessage("Считыватель #" + String(readerIndex + 1)  + "\n -> Тэг считан" + "\n\nUID (HEX) :\n" + content + "\nUID (DEC) :\n " + content10);
      //reader.mfrc522.PICC_DumpToSerial(&(reader.mfrc522.uid)); // Выводим дамп данных (считываем всё, что можно считать)
      
      // Мигаем светодиодами
      blink();
    }
  } else { // Если метка отсутствует
    
    if (reader.isPresent) {     // Если метка была на считывателе
      reader.isPresent = false; // Обновляем флаг наличия метки

      if (reader.lastUid != "") { // Если информация об этой метке ещё не была потёрта (т.е. актуальна)
        
        // Выводим сообщение о том, что метка была убрана
        printMessage("Считыватель #" + String(readerIndex + 1) + "\n -> Тэг убран" + "\n\nUID (HEX) :\n" + reader.lastUid + "\nUID (DEC) :\n " + reader.lastUid10); 

        // Мигаем светодиодами
        blink();
      }
    }
  }
}
/* Функции для NFC (конец) */
