// Подключение необходимых библиотек
#include <Wire.h>     // Библиотека для работы с I2C
#include <SPI.h>      // Библиотека для работы с SPI
#include <MFRC522.h>  // Библиотека для работы с RC522

// МАСШТАБИРУЕМОСТЬ: Указываем нужное количество считывателей
#define NUM_READERS 2   // Задаем количество считывателей
#define CHECK_TIMES 25  // Указываем количество проверок на наличие метки (во избежание ложных срабатываний)
#define CHECK_DELAY 100  // Указываем время задержки между проверками (для надёжности)

// Структура для удобного хранения данных о каждом считывателе
struct Reader {
  MFRC522 mfrc522;  // Объект считывателя
  String lastUid;   // Строка для хранения последнего считанного UID
  bool isPresent;   // Флаг наличия метки

  // Конструктор структуры, принимает пины SS (это SDA) и RST
  Reader(uint8_t ssPin, uint8_t rstPin) : mfrc522(ssPin, rstPin), isPresent(false) {}
};

// Создаем массив считывателей
// МАСШТАБИРУЕМОСТЬ: Задаём здесь подключения считывателей - (SDA, RST)
Reader readers[NUM_READERS] = {
  Reader(16, 5),
  Reader(17, 5),
  //Reader(17, 22)
};

// void setup - это самая первая функция
// Она вызывается (исполняется) когда на плату Arduino подается питание
void setup() {

  // Инициализируем встроенный светодиод
  pinMode(LED_BUILTIN, OUTPUT);

  Serial.begin(9600); // Инициализация серийного порта с скоростью 9600 бод
  while (!Serial);    // Для ESP32, чтобы дождаться инициализации Serial

  SPI.begin(); // Запускаем SPI интерфейс
  for (int i = 0; i < NUM_READERS; i++) { // Для каждого считывателя
    readers[i].mfrc522.PCD_Init(); // Инициализируем считыватель
  }

  // Выводим приветственное сообщение
  Serial.println(""); 
  Serial.println("...Модуль загрузился и начал работу...");
  Serial.println("Приложите вашу NFC-метку к RC522..."); 
}

// void loop - это основная функция
// Она будет выполняться бесконечно, после функции setup, пока не сядет батарейка, или плата не будет перезагружена.
void loop() {
  for (int i = 0; i < NUM_READERS; i++) { // Для каждого считывателя
    checkReader(readers[i], i);           // Проверяем, прочитал ли он новую метку
  }
}

// Функция для мигания светодиодом на ESP32
void blink() {
      digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
      delay(CHECK_DELAY);                      // Ждём CHECK_DELAY миллисекунд
      digitalWrite(LED_BUILTIN, LOW);   // turn the LED off by making the voltage LOW
      delay(CHECK_DELAY);                      // Ждём CHECK_DELAY миллисекунд
      digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
      delay(CHECK_DELAY);                      // Ждём CHECK_DELAY миллисекунд
      digitalWrite(LED_BUILTIN, LOW);   // turn the LED off by making the voltage LOW
}

// Функция проверяет наличие метки у считывателя
bool isCardPresent(Reader& reader) {
  for (int i = 0; i < CHECK_TIMES; i++) { // Проверяем наличие метки CHECK_TIMES раз

    // Если метка есть, возвращаем true
    if (reader.mfrc522.PICC_IsNewCardPresent() && reader.mfrc522.PICC_ReadCardSerial()) {
      return true;
    }

    delay(CHECK_DELAY); // Задержка между проверками в CHECK_DELAY миллисекунд
  }
  return false; // Если метка не найдена, возвращаем false
}

// Функция проверяет, была ли прочитана новая метка
void checkReader(Reader& reader, int readerIndex) {

  if (isCardPresent(reader)) {  // Если метка присутствует

    // Формирование строки с UID метки
    String content= "";         // Строка для хранения UID метки
    for (byte i = 0; i < reader.mfrc522.uid.size; i++) {                         // Для каждого байта UID
      content.concat(String(reader.mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ")); // Добавляем его к строке, предварительно преобразовав в HEX
      content.concat(String(reader.mfrc522.uid.uidByte[i], HEX));
    }

    // Если это новая метка
    if (content != reader.lastUid ) {
      
      if (reader.isPresent) {     // Если старая метка ещё числится за этим считывателем
        reader.isPresent = false; // Обновляем флаг наличия метки
      }

      // Обеспечиваем однозначность закрепления метки за одним считывателем
      for (int i = 0; i < NUM_READERS; i++) {                     // Для каждого другого считывателя
        if (i != readerIndex && readers[i].lastUid == content) {  // Если UID совпадает с UID последней прочитанной метки
          readers[i].lastUid = "";                                // "Забываем" этот UID
        }
      }

      // Закрепляем метку за данным считывателем
      reader.lastUid = content;   // Обновляем последний прочитанный UID
    }

    // Если это новая метка или метка была ранее удалена
    if (!reader.isPresent ) {
      reader.isPresent = true; // Обновляем флаг наличия метки
      Serial.println("Reader " + String(readerIndex + 1) + ": UID метки : " + content); // Выводим сообщение с номером считывателя и UID метки
      //reader.mfrc522.PICC_DumpToSerial(&(reader.mfrc522.uid)); // Выводим дамп данных (считываем всё, что можно считать)
      
      // Мигаем светодиодами
      blink();
    }
  } else { // Если метка отсутствует
    
    if (reader.isPresent) {     // Если метка была на считывателе
      reader.isPresent = false; // Обновляем флаг наличия метки

      if (reader.lastUid != "") { // Если информация об этой метке ещё не была потёрта (т.е. актуальна)
        Serial.println("Reader " + String(readerIndex + 1) + ": Метка " + reader.lastUid + " была убрана");  // Выводим сообщение о том, что метка была убрана

        // Мигаем светодиодами
        blink();
      }
    }
  }
}
