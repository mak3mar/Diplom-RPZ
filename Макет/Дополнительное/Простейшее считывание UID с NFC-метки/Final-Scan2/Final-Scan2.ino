/* Подключение и определение необходимого для самого ESP32 */
// Подключение необходимых библиотек для ESP32
#include "esp_task_wdt.h"  // Библиотека для работы с сторожевым таймером

// Объявляем константы для ESP32
#define WDT_TIMEOUT 120  // Таймаут сторожевого таймера в секундах
/* Подключение и определение необходимого для самого ESP32 (конец)*/


/* Подключение и определение необходимого для считывателей */
// Подключение необходимых библиотек для считывателей
#include <SPI.h>      // Библиотека для работы с SPI
#include <MFRC522.h>  // Библиотека для работы с RC522

// Объявляем константы для считывателей
// МАСШТАБИРУЕМОСТЬ: Указываем нужное количество считывателей
#define NUM_READERS 2  // Задаем количество считывателей

#define CHECK_TIMES 50   // Указываем количество проверок на наличие метки (во избежание ложных срабатываний)
#define CHECK_DELAY 100  // Указываем время задержки между проверками (для надёжности)

#define INIT_PERIOD 120000  // Период переинициализации считывателей в миллисекундах
uint32_t lastInitTime = 0;  // Время последней инициализации считывателей
/* Подключение и определение необходимого для считывателей (конец)*/


/* Подключение и определение необходимого для экрана */
// Подключение необходимых библиотек для экрана
#include <Wire.h>              // Библиотека для работы с I2C
#include <Adafruit_GFX.h>      // Библиотека Adafruit GFX
#include <Adafruit_SSD1306.h>  // Библиотека Adafruit SSD1306

// Объявляем константы для экрана
#define SCREEN_WIDTH 128  // Ширина OLED-дисплея
#define SCREEN_HEIGHT 64  // Высота OLED-дисплея
#define OLED_RESET -1     // На большинстве модулей OLED нет сигнала RESET
#define TEXT_SIZE 1       // Задаём размер шрифта

const unsigned long DISPLAY_DURATION = 15000;  // Продолжительность отображения сообщения (в миллисекундах)
unsigned long messageDisplayTime;              // Время отображения сообщения
/* Подключение и определение необходимого для экрана (конец)*/


/* Основные выводимые сообщения */
const char* HelloText = "\n....Модуль загрузился и начал работать....\n\n*** *** ** ** *** ***\n\n..Приложите вашу NFC-метку к считывателю..";  // Текст приветствия
const char* MainText = "\n...Модуль работает...\n\n*** *** ** ** *** ***\n\n..Приложите вашу NFC-метку к считывателю..";                        // Текст на главном экране
const char* InitNFC = "\nСчитыватели... ... ..\nпереинициализированы.\n\n*** *** ** ** *** ***\n\n..Приложите вашу NFC-метку к считывателю..";  // Текст на главном экране
/* Основные выводимые сообщения (конец) */


/* Определение структур и создание объектов */
// Структура для удобного хранения данных о каждом считывателе
struct Reader {
  MFRC522 mfrc522;   // Объект считывателя
  uint8_t rstPin;    // Pin rst
  uint8_t ssPin;     // Pin ss
  String lastUid;    // Строка для хранения последнего считанного UID в HEX
  String lastUid10;  // Строка для хранения последнего считанного UID в DEC
  bool isPresent;    // Флаг наличия метки

  // Конструктор структуры, принимает пины SS (это SDA) и RST
  Reader(uint8_t ssPin, uint8_t rstPin)
    : mfrc522(ssPin, rstPin), rstPin(rstPin), ssPin(ssPin), lastUid(""), lastUid10(""), isPresent(false) {}
};

// Создаем массив считывателей
// МАСШТАБИРУЕМОСТЬ: Задаём здесь подключения считывателей - (SDA, RST)
Reader readers[NUM_READERS] = {
  Reader(16, 5),
  Reader(17, 5),
  //Reader(17, 22)
};

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);  // Создаем объект экрана
/* Определение структур и создание объектов (конец) */


/* Основные функции */
// void setup - это самая первая функция
// Она вызывается (исполняется) когда на плату Arduino подается питание
void setup() {
  // Проводим инициализацию подфункций для самого ESP32
  esp_task_wdt_init(WDT_TIMEOUT, true);  // Инициализируем сторожевой таймер
  esp_task_wdt_add(NULL);                // Добавляем в сторожевой таймер основной цикл loop()

  // Инициализируем встроенный светодиод
  pinMode(LED_BUILTIN, OUTPUT);

  // Инициализация serial port
  Serial.begin(9600);  // Инициализация серийного порта с скоростью 9600 бод
  while (!Serial)
    ;  // Для ESP32, чтобы дождаться инициализации

  // Инициализация OLED дисплея (по интерфейсу I2C)
  initOled();

  // Инициализация SPI интерфейса и NFC-считывателей
  SPI.begin();
  initReaders(readers, NUM_READERS);  // Инициализация всех считывателей

  // Обработка (пере)загрузки и приветственное сообщение
  printResetReason();
  delay(5000);
  printMessage(HelloText);

  // Мигаем светодиодом, сигнализируя о включении
  blink();
}

// void loop - это основная функция
// Она будет выполняться бесконечно, после функции setup, пока не сядет батарейка, или плата не будет перезагружена.
void loop() {
  // Контролируем состояние ESP32 и компонентов
  esp_task_wdt_reset();  // Обновляем сторожевой таймер

  // Проверяем, не пора ли переинициализировать считыватели
  if (millis() - lastInitTime >= INIT_PERIOD) {
    initReaders(readers, NUM_READERS);  // Инициализируем все считыватели
    lastInitTime = millis();            // Обновляем время последней инициализации
  }

  // Обновляем графическое отображение
  printClear();  // Очищаем экран и выводим главное сообщение

  // Осуществляем проверку считывателей на наличие метки и считываем её (если есть)
  for (int i = 0; i < NUM_READERS; i++) {  // Для каждого считывателя
    checkReader(readers[i], i);            // Проверяем, прочитал ли он новую метку
  }
}
/* Основные функции (конец) */


/* Функции инициализации */
// Функция инициализации OLED экрана (по интерфейсу I2C)
void initOled() {
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {  // Инициализация с I2C адресом 0x3C
    Serial.println(F("SSD1306 (OLED): сбой инициализации"));
    for (;;)
      ;  // Если инициализация не удалась, зацикливаемся (не тестировалось)
  }
  display.cp437(true);     // Ставим русскую кодировку
  display.clearDisplay();  // Очищаем экран
  display.display();       // Обновляем экран

  display.setTextSize(TEXT_SIZE);  // Задаем размер текста
  display.setTextColor(WHITE);     // Задаем цвет текста
}

// Функция для запуска и перезапуска считывателей
/*  Примечание: Данная функция была выведена эмперическим путём.
    При другом способе инициализации один из считывателей периодически
    (при одном из запусков) отказывался считывать метки */
void initReaders(Reader readers[], int numReaders) {
  for (int i = 0; i < numReaders; i++) {    // Для каждого считывателя
    pinMode(readers[i].rstPin, OUTPUT);     // Задаем pin rstPin как OUTPUT
    digitalWrite(readers[i].rstPin, LOW);   // Проводим перезагрузку (reset) считывателя
    delay(50);                              // Пауза после reset
    digitalWrite(readers[i].rstPin, HIGH);  // Возвращаем pin rstPin в HIGH
    readers[i].mfrc522.PCD_Init();          // Инициализируем считыватель
  }
  for (int i = 0; i < numReaders; i++) {       // Для каждого считывателя
    readers[i].mfrc522.PCD_PerformSelfTest();  // Проводим самотестирование (я не уверен, что эта функция работает...)
    readers[i].mfrc522.PCD_Init();             // Инициализируем считыватель
  }
  printMessage(InitNFC);

  // Обеспечиваем пересчитывание
  for (int i = 0; i < NUM_READERS; i++) {  // Для каждого другого считывателя
    readers[i].lastUid = "";               // "Забываем" этот UID
    readers[i].lastUid10 = "";
  }
}

// Функция вывода причины перезагрузки
void printResetReason() {
  // Узнаваем причину перезагрузки
  esp_reset_reason_t reason = esp_reset_reason();

  // Выводим причину
  switch (reason) {
    case ESP_RST_WDT:  // Сброс по сторожевому таймеру
      printMessage("(Пере)загрузка:\nСброс по сторожевому таймеру");
      break;
    case ESP_RST_DEEPSLEEP:  // Сброс после выхода из режима глубокого сна
      printMessage("(Пере)загрузка:\nСброс после выхода из режима глубокого сна");
      break;
    case ESP_RST_BROWNOUT:  // Сброс из-за просадки напряжения
      printMessage("(Пере)загрузка:\nСброс из-за просадки напряжения");
      break;
    case ESP_RST_SW:  // Программный сброс (вызван esp_restart() или подобной функцией)
      printMessage("(Пере)загрузка:\nПрограммный сброс");
      break;
    case ESP_RST_PANIC:  // Сброс из-за ошибки в коде (panic)
      printMessage("(Пере)загрузка:\nСброс из-за ошибки в коде");
      break;
    case ESP_RST_INT_WDT:  // Сброс из-за аппаратного сторожевого таймера
      printMessage("(Пере)загрузка:\nСброс по аппаратному сторожевому таймеру");
      break;
    case ESP_RST_TASK_WDT:  // Сброс из-за сторожевого таймера задач
      printMessage("(Пере)загрузка:\nСброс по сторожевому таймеру задач");
      break;
    case ESP_RST_EXT:  // Сброс по внешнему сигналу (not applicable for ESP32)
      printMessage("(Пере)загрузка:\nСброс по внешнему сигналу");
      break;
    case ESP_RST_SDIO:  // Сброс по SDIO
      printMessage("(Пере)загрузка:\nСброс по SDIO");
      break;
      /*case ESP_RST_USB:  // Сброс по USB 
      printMessage("(Пере)загрузка:\nСброс по USB");
      break;*/
    case ESP_RST_POWERON:  // Включение по подаче питания
      printMessage("(Пере)загрузка:\nПодача питания\n\nВозможно перезагрука\nпо кнопке рестарта?");
      break;
    case ESP_RST_UNKNOWN:  // Сброс по внешнему сигналу
      printMessage("(Пере)загрузка:\nНе известно");
      break;
    default:
      printMessage("(Пере)загрузка:\nНе известно");
      break;
  }
}
/* Функции инициализации (конец)*/


/* Декоративные функции */
// Функция для мигания светодиодом на ESP32
void blink() {
  digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
  delay(CHECK_DELAY);               // Ждём CHECK_DELAY миллисекунд
  digitalWrite(LED_BUILTIN, LOW);   // turn the LED off by making the voltage LOW
  delay(CHECK_DELAY);               // Ждём CHECK_DELAY миллисекунд
  digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
  delay(CHECK_DELAY);               // Ждём CHECK_DELAY миллисекунд
  digitalWrite(LED_BUILTIN, LOW);   // turn the LED off by making the voltage LOW
}

// Функция вывода на OLED экран и на serial port
void printMessage(String message) {
  display.clearDisplay();             // Очищаем экран
  display.setCursor(0, 0);            // Устанавливаем курсор в начало
  display.println(utf8rus(message));  // Выводим сообщение, прредварительно преобразовав кирилицу в читаемый вид
  display.display();                  // Обновляем экран

  messageDisplayTime = millis();  // Запоминаем время вывода сообщения

  Serial.println(message);  // Выводим сообщение в serial port
}

// Функция очистки OLED экрана
void printClear() {

  // Проверяем, прошло ли достаточно времени с момента вывода сообщения
  if (messageDisplayTime != 0 && millis() - messageDisplayTime >= DISPLAY_DURATION) {
    printMessage(MainText);  // Сообщение по умолчанию - главный экран
    messageDisplayTime = 0;
  }
}

/* Recode russian fonts from UTF-8 to Windows-1251 */
String utf8rus(String source) {
  int i, k;
  String target = "";
  unsigned char n;
  char m[2] = { '0', '\0' };

  k = source.length();
  i = 0;

  while (i < k) {
    n = source[i];
    i++;

    if (n >= 0xC0) {
      switch (n) {
        case 0xD0:
          {
            n = source[i];
            i++;
            if (n == 0x81) {
              n = 0xA8;
              break;
            }
            if (n >= 0x90 && n <= 0xBF) n = n + 0x30;
            break;
          }
        case 0xD1:
          {
            n = source[i];
            i++;
            if (n == 0x91) {
              n = 0xB8;
              break;
            }
            if (n >= 0x80 && n <= 0x8F) n = n + 0x70;
            break;
          }
      }
    }
    m[0] = n;
    target = target + String(m);
  }
  return target;
}
/* Декоративные функции (конец) */


/* Функции для NFC */
// Функция проверяет наличие метки у считывателя
bool isCardPresent(Reader& reader) {

  /* Тестовый Участок */
  byte atqa_answer[2];
  byte atqa_size = 2;
  reader.mfrc522.PICC_WakeupA(atqa_answer, &atqa_size);
  /* Тестовый Участок (конец)*/

  for (int i = 0; i < CHECK_TIMES; i++) {  // Проверяем наличие метки CHECK_TIMES раз
    // Если метка есть, возвращаем true
    if (reader.mfrc522.PICC_IsNewCardPresent() && reader.mfrc522.PICC_ReadCardSerial()) {
      return true;
    }

    delay(CHECK_DELAY);  // Задержка между проверками в CHECK_DELAY миллисекунд
  }
  return false;  // Если метка не найдена, возвращаем false
}

// Функция проверяет, была ли прочитана новая метка
void checkReader(Reader& reader, int readerIndex) {

  if (isCardPresent(reader)) {  // Если метка присутствует

    // Формирование строки с UID метки
    String content = "";                                                          // Строка для хранения UID метки в HEX
    String content10 = "";                                                        // Строка для хранения UID метки в DEC
    for (byte i = 0; i < reader.mfrc522.uid.size; i++) {                          // Для каждого байта UID
      content.concat(String(reader.mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));  // Добавляем его к строке, предварительно преобразовав в HEX
      content.concat(String(reader.mfrc522.uid.uidByte[i], HEX));
      content10.concat(String(reader.mfrc522.uid.uidByte[i]));
    }

    // Если это новая метка
    if (content != reader.lastUid) {

      if (reader.isPresent) {      // Если старая метка ещё числится за этим считывателем
        reader.isPresent = false;  // Обновляем флаг наличия метки
      }

      // Обеспечиваем однозначность закрепления метки за одним считывателем
      for (int i = 0; i < NUM_READERS; i++) {                     // Для каждого другого считывателя
        if (i != readerIndex && readers[i].lastUid == content) {  // Если UID совпадает с UID последней прочитанной метки
          readers[i].lastUid = "";                                // "Забываем" этот UID
          readers[i].lastUid10 = "";
        }
      }

      // Закрепляем метку за данным считывателем
      reader.lastUid = content;  // Обновляем последний прочитанный UID
      reader.lastUid10 = content10;
    }

    // Если это новая метка или метка была ранее удалена
    if (!reader.isPresent) {
      reader.isPresent = true;  // Обновляем флаг наличия метки

      // Выводим сообщение с номером считывателя и UID метки
      printMessage("Считыватель #" + String(readerIndex + 1) + "\n -> Метка считана" + "\n\nUID (HEX) :\n" + content + "\nUID (DEC) :\n " + content10);
      //reader.mfrc522.PICC_DumpToSerial(&(reader.mfrc522.uid)); // Выводим дамп данных (считываем всё, что можно считать)

      // Мигаем светодиодами
      blink();
    }
  } else {  // Если метка отсутствует

    if (reader.isPresent) {      // Если метка была на считывателе
      reader.isPresent = false;  // Обновляем флаг наличия метки

      if (reader.lastUid != "") {  // Если информация об этой метке ещё не была потёрта (т.е. актуальна)

        // Выводим сообщение о том, что метка была убрана
        printMessage("Считыватель #" + String(readerIndex + 1) + "\n -> Метка убрана" + "\n\nUID (HEX) :\n" + reader.lastUid + "\nUID (DEC) :\n " + reader.lastUid10);

        // Мигаем светодиодами
        blink();
      }
    }
  }
}
/* Функции для NFC (конец) */
