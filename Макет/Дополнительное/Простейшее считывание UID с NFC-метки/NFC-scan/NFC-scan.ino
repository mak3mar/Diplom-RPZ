// Подключение необходимых библиотек
#include <Wire.h> // Библиотека для работы с I2C
#include <SPI.h> // Библиотека для работы с SPI
#include <MFRC522.h> // Библиотека для работы с RC522

#define SS_PIN 21 // Назначение пина для SDA (сигнал Slave Select)
#define RST_PIN 22 // Назначение пина для RST (сброс)

MFRC522 mfrc522(SS_PIN, RST_PIN); // Создание экземпляра объекта MFRC522 с заданными пинами
String lastUid; // Строковая переменная для хранения последнего считанного UID

void setup() {
  Serial.begin(9600); // Инициализация серийного порта с скоростью 9600 бод
  while (!Serial); // Для ESP32, чтобы дождаться инициализации Serial
  
  SPI.begin(); // Инициализация SPI интерфейса
  mfrc522.PCD_Init(); // Инициализация модуля RC522

  Serial.println("Приложите вашу NFC-метку к RC522..."); // Вывод сообщения в сериальный порт
}

void loop() {
  // Проверка, есть ли новая карта для чтения
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return; // Если новой карты нет, выходим из функции loop()
  }
  
  // Выбираем одну из карт
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return; // Если не удалось считать карту, выходим из функции loop()
  }
  
  // Создаем строку UID
  String content= "";
  for (byte i = 0; i < mfrc522.uid.size; i++) {
     // Добавляем в строку содержимое каждого байта UID
     content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
     content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  
  // Если UID не изменился, пропускаем его
  if (content == lastUid) {
    return; // Если UID такой же, как и последний считанный, выходим из функции loop()
  }

  // Сохраняем новый UID и выводим его
  lastUid = content; // Сохраняем новый считанный UID
  
  Serial.println("UID тэга : " + content); // Выводим новый UID в сериальный порт
}
