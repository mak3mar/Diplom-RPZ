// Подключение необходимых библиотек
#include <Wire.h> // Библиотека для работы с I2C
#include <SPI.h> // Библиотека для работы с SPI
#include <MFRC522.h> // Библиотека для работы с RC522

// Задаем количество считывателей
// МАСШТАБИРУЕМОСТЬ: Указываем нужное количество считывателей
#define NUM_READERS 2

// Структура для удобного хранения данных о каждом считывателе
struct Reader {
  MFRC522 mfrc522; // Объект считывателя
  String lastUid;  // Строка для хранения последнего считанного UID

  // Конструктор структуры, принимает пины SS (это SDA) и RST
  Reader(uint8_t ssPin, uint8_t rstPin) : mfrc522(ssPin, rstPin) {}
};

// Создаем массив считывателей
// МАСШТАБИРУЕМОСТЬ: Задаём здесь подключения считывателей - (SDA, RST)
Reader readers[NUM_READERS] = {
  Reader(21, 22),
  Reader(5, 22),
  //Reader(17, 22)
};

// void setup - это самая первая функция
// Она вызывается (исполняется) когда на плату Arduino подается питание
void setup() {
  Serial.begin(9600);   // Инициализация серийного порта с скоростью 9600 бод
  while (!Serial);      // Для ESP32, чтобы дождаться инициализации Serial

  SPI.begin(); // Запускаем SPI интерфейс
  for (int i = 0; i < NUM_READERS; i++) { // Для каждого считывателя
    readers[i].mfrc522.PCD_Init();        // Инициализируем считыватель
  }

  Serial.println("Приложите вашу NFC-метку к RC522..."); // Выводим сообщение
}

// void loop - это основная функция
// Она будет выполняться снова и снова, пока не сядет батарейка, или плата не будет перезагружена.
void loop() {
  for (int i = 0; i < NUM_READERS; i++) { // Для каждого считывателя
    checkReader(readers[i], i);           // Проверяем, прочитал ли он новую метку
  }
}

// Функция для считывания информации с метки
void checkReader(Reader& reader, int readerIndex) {
  
  // Если считыватель не видит новую метку, выходим из функции
  if ( ! reader.mfrc522.PICC_IsNewCardPresent()) {
    return;
  }

  // Если считыватель не может прочитать метку, выходим из функции
  if ( ! reader.mfrc522.PICC_ReadCardSerial()) {
    return;
  }

  String content= ""; // Строка для хранения UID метки
  for (byte i = 0; i < reader.mfrc522.uid.size; i++) { // Для каждого байта UID
     // Добавляем его к строке, предварительно преобразовав в HEX
     content.concat(String(reader.mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
     content.concat(String(reader.mfrc522.uid.uidByte[i], HEX));
  }

  // Если считанный UID совпадает с последним прочитанным, выходим из функции
  if (content == reader.lastUid) {
    return;
  }

  // Для всех других считывателей
  for (int i = 0; i < NUM_READERS; i++) {
    // Если этот UID был последним считанным, "забываем" его
    if (i != readerIndex && readers[i].lastUid == content) {
      readers[i].lastUid = "";
    }
  }

  // Обновляем последний считанный UID для текущего считывателя
  reader.lastUid = content;

  // Выводим сообщение с номером считывателя и UID метки
  Serial.println("Reader " + String(readerIndex + 1) + ": UID тэга : " + content);
}

